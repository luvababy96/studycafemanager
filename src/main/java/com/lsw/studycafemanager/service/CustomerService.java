package com.lsw.studycafemanager.service;

import com.lsw.studycafemanager.entity.StudyCafeCustomer;
import com.lsw.studycafemanager.model.CustomerChangeUseTimeRequest;
import com.lsw.studycafemanager.model.CustomerItem;
import com.lsw.studycafemanager.model.CustomerRequest;
import com.lsw.studycafemanager.repository.StudyCafeCustomerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class CustomerService {

    private final StudyCafeCustomerRepository studyCafeCustomerRepository;

    public void setCustomer(CustomerRequest request) {
        StudyCafeCustomer addData = new StudyCafeCustomer();
        addData.setCustomerName(request.getCustomerName());
        addData.setCustomerPhone(request.getCustomerPhone());
        addData.setBirthday(request.getBirthday());
        addData.setSeatNumber(request.getSeatNumber());
        addData.setUseTime(request.getUseTime());
        addData.setCheckInTime(LocalDateTime.now());
        addData.setUseEndingTime(LocalDateTime.now().plusHours(request.getUseTime()));
        addData.setRentCage(request.getRentCage());
        addData.setBuyTicket(request.getBuyTicket());

        studyCafeCustomerRepository.save(addData);

    }

    public List<CustomerItem> getCustomers() {
        List<StudyCafeCustomer> originList = studyCafeCustomerRepository.findAll();

        List<CustomerItem> result = new LinkedList<>();

        for(StudyCafeCustomer item : originList) {
            CustomerItem addItem = new CustomerItem();
            addItem.setId(item.getId());
            addItem.setCustomerName(item.getCustomerName());
            addItem.setCustomerPhone(item.getCustomerPhone());
            addItem.setBirthday(item.getBirthday());
            addItem.setSeatNumber(item.getSeatNumber());
            addItem.setUseTime(item.getUseTime());
            addItem.setCheckInTime(item.getCheckInTime());
            addItem.setUseEndingTime(item.getUseEndingTime());
            addItem.setCheckOutTime(item.getCheckOutTime());
            addItem.setExtraService("사물함-" + item.getRentCage() + "/" + "커피쿠폰-" + item.getBuyTicket());

            result.add(addItem);
        }
        return result;
    }

    public void putUseTime(long id, CustomerChangeUseTimeRequest request) {
        StudyCafeCustomer originData = studyCafeCustomerRepository.findById(id).orElseThrow();
        originData.setUseTime(request.getUseTime());
        originData.setUseEndingTime(originData.getCheckInTime().plusHours(request.getUseTime()));

        studyCafeCustomerRepository.save(originData);
    }

    public void putCheckOut(long id) {
        StudyCafeCustomer originData = studyCafeCustomerRepository.findById(id).orElseThrow();
        originData.setCheckOutTime(LocalDateTime.now());

        studyCafeCustomerRepository.save(originData);

    }

    public void delCustomer(long id) {studyCafeCustomerRepository.deleteById(id); }

}
