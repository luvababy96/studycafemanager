package com.lsw.studycafemanager.controller;

import com.lsw.studycafemanager.model.CustomerChangeUseTimeRequest;
import com.lsw.studycafemanager.model.CustomerItem;
import com.lsw.studycafemanager.model.CustomerRequest;
import com.lsw.studycafemanager.service.CustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/customer")
public class CustomerController {
    private final CustomerService customerService;

    @PostMapping("/data")
    public String setCustomer(@RequestBody @Valid CustomerRequest request) {
        customerService.setCustomer(request);
        return "OK";
    }

    @GetMapping("/all")
    public List<CustomerItem> getCustomers() {
        List<CustomerItem> result = customerService.getCustomers();

        return result;

    }

    @PutMapping("/change-use-time/id/{id}")
    public String putUseTime(@PathVariable long id, @RequestBody @Valid CustomerChangeUseTimeRequest request) {
        customerService.putUseTime(id, request);

        return "OK";
    }

    @PutMapping("/check-out/id/{id}")
    public String putCheckOut(@PathVariable long id) {
        customerService.putCheckOut(id);

        return "OK";
    }

    @DeleteMapping("/sign-out/id/{id}")
    public String delCustomer(@PathVariable long id) {
        customerService.delCustomer(id);

        return "OK";
    }


}
