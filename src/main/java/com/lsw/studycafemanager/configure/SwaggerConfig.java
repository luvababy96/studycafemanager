package com.lsw.studycafemanager.configure;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Configuration
@EnableSwagger2

public class SwaggerConfig {
    private String version;
    private String title;
    private final String TITLE_FIX = "스터디 카페 고객 관리 API "; //  "보안 예제 API "을 프로젝트이름에 맞게 변경하여 작성

    @Bean
    public Docket apiV1() {
        version = "V1";
        title = TITLE_FIX + version;

        return new Docket(DocumentationType.SWAGGER_2)
                .securityContexts(Collections.singletonList(securityContext()))
                .securitySchemes(Collections.singletonList(getApiKey()))
                .useDefaultResponseMessages(false)
                .groupName(version)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.lsw.studycafemanager")) //apisecuritysample를 해당 프로젝트 이름으로 수정하기
                .paths(PathSelectors.ant("/v1/**")) // controller에 있는 mapping 이름 처음에 /v1붙이기
                .build()
                .apiInfo(getApiInfo(title, version))
                .enable(true);
    }

    @Bean
    public Docket apiV2() {
        version = "V2";
        title = TITLE_FIX + version;

        return new Docket(DocumentationType.SWAGGER_2)
                .securityContexts(Collections.singletonList(securityContext()))
                .securitySchemes(Collections.singletonList(getApiKey()))
                .useDefaultResponseMessages(false)
                .groupName(version)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.lsw.studycafemanager")) //apisecuritysample를 해당 프로젝트 이름으로 수정하기
                .paths(PathSelectors.ant("/v2/**"))
                .build()
                .apiInfo(getApiInfo(title, version))
                .enable(true);
    }

    private ApiInfo getApiInfo(String title, String version) {
        return new ApiInfo(
                title,
                "Swagger API Docs",
                version,
                "SOWOOLEE.com",
                new Contact("lsw", "SOWOOLEE.com", "luvababy96@gmail.com"),
                "Licenses",
                "SOWOOLEE.com",
                new ArrayList<>()
        );
    }

    private SecurityContext securityContext() {
        return SecurityContext.builder()
                .securityReferences(defaultAuth())
                .build();
    }

    private List<SecurityReference> defaultAuth() {
        AuthorizationScope authorizationScope = new AuthorizationScope("global", "accessEverything");
        AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
        authorizationScopes[0] = authorizationScope;
        return List.of(new SecurityReference(HttpHeaders.AUTHORIZATION, authorizationScopes));
    }

    private ApiKey getApiKey() {
        return new ApiKey("Authorization", HttpHeaders.AUTHORIZATION, "header");
    }

}
