package com.lsw.studycafemanager.repository;

import com.lsw.studycafemanager.entity.StudyCafeCustomer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudyCafeCustomerRepository extends JpaRepository<StudyCafeCustomer,Long> {
}
