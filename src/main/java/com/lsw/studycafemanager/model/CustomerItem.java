package com.lsw.studycafemanager.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter
public class CustomerItem {

    private Long id;
    private String customerName;
    private String customerPhone;
    private LocalDate birthday;
    private String seatNumber;
    private Integer useTime;
    private LocalDateTime checkInTime;
    private LocalDateTime useEndingTime;
    private LocalDateTime checkOutTime;
    private String extraService;


}
