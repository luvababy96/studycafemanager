package com.lsw.studycafemanager.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter
public class CustomerRequest {

    @NotNull
    @Length(min = 2, max = 20)
    private String customerName;

    @NotNull
    @Length(min = 7, max = 20)
    private String customerPhone;

    @NotNull
    private LocalDate birthday;

    @NotNull
    @Length(min = 2, max = 20)
    private String seatNumber;

    @NotNull
    @Min(value = 1)
    @Max(value = 24)
    private Integer useTime;

    @NotNull
    private Boolean rentCage;

    @NotNull
    private Boolean buyTicket;


}
