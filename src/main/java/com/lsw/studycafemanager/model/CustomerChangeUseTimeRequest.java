package com.lsw.studycafemanager.model;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class CustomerChangeUseTimeRequest {

    @NotNull
    @Min(value = 1)
    @Max(value = 24)
    private Integer useTime;

}
